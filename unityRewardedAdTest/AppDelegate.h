//
//  AppDelegate.h
//  unityRewardedAdTest
//
//  Created by Vasiliy Korchagin on 25.06.2020.
//  Copyright © 2020 Vasiliy Korchagin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>


@end

