//
//  ViewController.m
//  unityRewardedAdTest
//
//  Created by Vasiliy Korchagin on 25.06.2020.
//  Copyright © 2020 Vasiliy Korchagin. All rights reserved.
//

#import "ViewController.h"
#import <UnityAds/UnityAds.h>

NSString * const GameId = @"";
NSString * const PlacementId = @"";

@interface ViewController () <UnityAdsDelegate>

@property (nonatomic, strong) UIButton *showButton;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [UnityAds initialize:GameId testMode:YES];
    [UnityAds addDelegate:self];
    
    self.showButton = [[UIButton alloc] init];
    [self.showButton setTitle:@"Show Ads" forState:UIControlStateNormal];
    [self.showButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.showButton addTarget:self action:@selector(showAds) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.showButton];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSLog(@"!__ viewDidAppear");
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    [self.showButton sizeToFit];
    self.showButton.center = self.view.center;
}

- (void)showAds {
    if ([UnityAds isReady:PlacementId]) {
        [UnityAds show:self placementId:PlacementId];
    } else {
        NSLog(@"!__ UnityAds not ready");
    }
}

#pragma mark - UnityAdsDelegate

- (void)unityAdsReady:(NSString *)placementId {
    // Do nothing
}

- (void)unityAdsDidError:(UnityAdsError)error withMessage:(NSString *)message {
    NSLog(@"!__ unityAdsDidError with message %@", message);
}

- (void)unityAdsDidStart:(NSString *)placementId {
    NSLog(@"!__ unityAdsDidStart %@", placementId);
}

- (void)unityAdsDidFinish:(NSString *)placementId
          withFinishState:(UnityAdsFinishState)state {
    NSLog(@"!__ unityAdsDidFinish %@ withFinishState %ld(long)", placementId, state);
}

@end
